import './style.scss'

$(document).ready(function () {
    let sum = 0
    let target = $('.number-panel input')
    target.val('')

    $('.coins-list .coins-item button').on('click', function(){
        let value = parseInt($(this).parent().attr('data-coin'))
        sum += value

        $(".vendor-machine-panel .display span").html(sum)
    })

    $('.vendor-machine-panel .cancel-button button').on('click', function() {
        let coins = [200,100,50, 20, 10, 5]
        let returns = []
        let images = []

        while (sum > 0) {
            $.each(coins, function(index, value) {
                if(sum == value) {
                    sum = sum - value
                    returns.push(value)
                    return false;
                } else if(value > sum) {
                    return true;
                } else if(value < sum) {
                    sum = sum - value
                    returns.push(value)
                    return false;
                }
            })
        }

        $.each(returns, function(index, value) {
            console.log(value)
            images.push('<img src="./img/coin-'+ value +'.png" alt="">')
        })
        $('.coins-back').html(images.join(''))

        setTimeout(function() {
            $('.coins-back img').fadeOut().remove()
        }, 3000)

        sum = 0

        $(".vendor-machine-panel .display span").html(sum)
    })


    $('.number-panel *[data-submit]').on('click', function() {
        let number = target.val()
        let item = $('.vendor-machine-table *[data-number='+number+'] img.available')
        let price = parseInt($('.vendor-machine-table *[data-number='+number+']').data('price'))

        console.log(item)

        sum-= price

        $(".vendor-machine-panel .display span").html(sum)

        item.eq(0).addClass('dropped').removeClass('available')

        let droppedItem = $('.vendor-machine-table *[data-number='+ number +'] img.dropped')

        droppedItem.appendTo('.finish-can')

        setTimeout(function() {
            $('.finish-can img').eq(0).fadeOut(300).remove()
        }, 3000)

        if(sum < 0) {
            alert('you don\'t have enought coins')
        }
    })

    $('.number-panel button[data-button]').on('click', function() {
        let value = parseInt($(this).data('button'))
        target.val(`${target.val()}${value}`)
    })

    $('.number-panel button[data-cancel]').on('click', function() {
        target.val('')
    })
});
